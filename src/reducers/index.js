import accountsReducer from "./accountsReducer";
import { combineReducers } from "redux";
import { reducer as formReducer } from "redux-form";

const rootReducer = combineReducers({
  accounts: accountsReducer,
  form: formReducer
});

export default rootReducer;
