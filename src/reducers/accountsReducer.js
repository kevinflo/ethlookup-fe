import * as types from "../actions/actionTypes";

export default (state = [], action) => {
  var payload = action.payload;
  switch (action.type) {
    case types.ACCOUNT_REQUEST:
      return {
        ...state,
        [payload.address]: {
          pending: true
        }
      };
    case types.ACCOUNT_SUCCESS:
      return {
        ...state,
        [payload.address]: {
          pending: false,
          transactions: payload.transactions,
          balance: payload.balance
        }
      };
    case types.ACCOUNT_FAILURE:
      return {
        ...state,
        [payload.address]: {
          pending: false,
          error: payload.error
        }
      };
    default:
      return state;
  }
};
