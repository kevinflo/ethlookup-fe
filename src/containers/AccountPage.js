import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as accountActions from "../actions/accountActions";
import SearchInput from "../components/SearchInput";
import FilterInput from "../components/FilterInput";
import TransactionsList from "../components/TransactionsList";
import AddressInfo from "../components/AddressInfo";

class AccountPage extends Component {
  componentDidMount() {
    this.props.actions.loadAccount(this.props.match.params.address);
  }
  render() {
    const { transactions } = this.props;
    return (
      <div>
        <SearchInput />
        <AddressInfo
          pending={this.props.pending}
          balance={this.props.balance}
          match={this.props.match}
        />
        <small>
          Filter by transactions involving a specific address (optional):
        </small>
        <FilterInput />
        <h2>Transactions:</h2>
        {this.props.pending && "Loading..."}
        {!this.props.pending && (
          <TransactionsList transactions={transactions} />
        )}
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  var address = ownProps.match.params.address;
  var accountData = {};
  var transactions = [];
  var balance = 0;
  var pending = true;

  if (address && (state.accounts && state.accounts[address])) {
    accountData = state.accounts[address];
  }

  if (accountData.balance) {
    balance = accountData.balance;
  }

  if (accountData.pending === false) {
    pending = false;
  }

  if (accountData.transactions) {
    transactions = accountData.transactions;
  }

  return {
    transactions,
    pending,
    accountData,
    balance
  };
};

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(accountActions, dispatch)
  };
}

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(AccountPage)
);
