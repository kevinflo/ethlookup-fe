import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import SearchInput from "../components/SearchInput";
import { Jumbotron } from "react-bootstrap";

class SearchPage extends Component {
  render() {
    return (
      <div className="page">
        <Jumbotron>
          <h1>Ethlookup.com</h1>
          <p>The simplest Ethereum address lookup site in the world</p>
        </Jumbotron>
        <SearchInput />
      </div>
    );
  }
}

export default withRouter(SearchPage);
