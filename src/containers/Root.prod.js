import React from "react";
import { Provider } from "react-redux";
import { Route } from "react-router-dom";
import SearchPage from "./SearchPage";
import AccountPage from "./AccountPage";
import { Grid, Row, Col } from "react-bootstrap";
import AppNavbar from "../components/AppNavbar";

const Root = ({ store }) => {
  return (
    <Provider store={store}>
      <div>
        <AppNavbar />
        <Grid id="app-holder">
          <Row>
            <Col xs={12}>
              <Route exact path="/" component={SearchPage} />
              <Route path="/:address" component={AccountPage} />
            </Col>
          </Row>
        </Grid>
      </div>
    </Provider>
  );
};

export default Root;
