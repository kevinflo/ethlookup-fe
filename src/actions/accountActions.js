import * as types from "./actionTypes";
import axios from "axios";

export function loadAccount(ethAddress, filterAddress) {
  return async function(dispatch, getState) {
    dispatch(accountRequest(ethAddress));
    var path = `/api/addresses/${ethAddress}`;

    if (filterAddress) {
      path += `?filter=${filterAddress}`;
    }

    axios
      .get(path)
      .then(response => {
        if (!response || !response.data || !response.data.balance) {
          dispatch(accountFailure(ethAddress, "An error has occurred"));
        } else {
          var transactions = response.data.transactions;
          if (!Array.isArray(transactions)) {
            // Just one sanity check because of how finnicky etherscan's API is for accounts with no transactions
            transactions = [];
          }

          transactions.sort((a, b) => {
            return parseInt(b.timeStamp, 10) - parseInt(a.timeStamp, 10);
          });

          dispatch(
            accountSuccess(ethAddress, response.data.balance, transactions)
          );
        }
      })
      .catch(function(error) {
        dispatch(accountFailure(ethAddress, error));
      });
  };
}

const accountRequest = address => {
  return {
    type: types.ACCOUNT_REQUEST,
    payload: { address }
  };
};

const accountSuccess = (address, balance, transactions) => {
  return {
    type: types.ACCOUNT_SUCCESS,
    payload: {
      address,
      balance,
      transactions
    }
  };
};
const accountFailure = (address, error) => {
  return {
    type: types.ACCOUNT_FAILURE,
    payload: {
      address,
      error
    }
  };
};
