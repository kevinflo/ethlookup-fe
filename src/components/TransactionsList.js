import React, { Component } from "react";
import { Table } from "react-bootstrap";
import Transaction from "./Transaction";

class TransactionsList extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <Table responsive>
        <thead>
          <tr>
            <th>TxHash</th>
            <th>Block</th>
            <th>From</th>
            <th>To</th>
            <th>Value</th>
          </tr>
        </thead>
        <tbody>
          {this.props.transactions.map(transaction => (
            <Transaction transaction={transaction} key={transaction.hash} />
          ))}
        </tbody>
      </Table>
    );
  }
}

export default TransactionsList;
