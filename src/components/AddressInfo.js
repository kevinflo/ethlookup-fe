import React from "react";
import { PageHeader } from "react-bootstrap";
var etherscanAddressUrlBase = "https://etherscan.io/address/";

const AddressInfo = props => (
  <PageHeader>
    Address:{" "}
    <small class="address">
      <a
        href={etherscanAddressUrlBase + props.match.params.address}
        target="_blank"
        rel="noopener noreferrer"
      >
        {props.match.params.address}
      </a>
    </small>
    <br />
    Balance: {props.pending && <small>Loading...</small>}{" "}
    {!props.pending && (
      <small>{props.balance / 1000000000000000000 + " Ether"}</small>
    )}
  </PageHeader>
);

export default AddressInfo;
