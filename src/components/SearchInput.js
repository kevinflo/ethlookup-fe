import { Field, reduxForm } from "redux-form";
import { connect } from "react-redux";
import React, { Component } from "react";
import { Button, Form, FormControl } from "react-bootstrap";
import { LinkContainer } from "react-router-bootstrap";
import { withRouter } from "react-router-dom";

const validate = values => {
  const errors = {};
  if (values.ethAddress && values.ethAddress.length !== 42) {
    errors["ethAddress"] = "Incorrect address length";
  }

  return errors;
};

const renderInput = ({
  input,
  label,
  placeholder,
  id,
  meta: { touched, error, submitted },
  ...custom
}) => {
  return (
    <div>
      <FormControl
        margin="normal"
        label={label}
        {...input}
        {...custom}
        type="text"
        placeholder={placeholder}
        id={id}
      />
      {touched && error && submitted && <span color="error">{error}</span>}
    </div>
  );
};

class SearchInput extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  handleSubmit(event) {
    event.preventDefault();
    this.props.history.push("/" + this.props.ethAddress);
  }

  render() {
    const { invalid, pristine, ethAddress } = this.props;

    return (
      <Form onSubmit={this.handleSubmit.bind(this)}>
        <Field
          component={renderInput}
          type="text"
          name="ethAddress"
          id="eth-address"
          placeholder="Ethereum address (ex: 0xa64CeC5f0247da31A6212709EbAc53b9d3b9B0e3)"
          bsSize="large"
        />
        <LinkContainer to={`/${ethAddress}`}>
          <Button
            id="btn-search"
            bsStyle="primary"
            bsSize="large"
            block
            disabled={pristine || invalid}
          >
            Search Address
          </Button>
        </LinkContainer>
      </Form>
    );
  }
}

function mapStateToProps(state, props) {
  return {
    ethAddress:
      state.form.searchInput &&
      state.form.searchInput.values &&
      state.form.searchInput.values.ethAddress
  };
}

export default withRouter(
  reduxForm({
    form: "searchInput",
    validate
  })(connect(mapStateToProps)(SearchInput))
);
