import React from "react";

var etherscanAddressUrlBase = "https://etherscan.io/address/";
var etherscanTransactionUrlBase = "https://etherscan.io/tx/";

const Transaction = props => (
  <tr>
    <td class="constrained">
      <span>
        <a
          href={etherscanTransactionUrlBase + props.transaction.hash}
          target="_blank"
          rel="noopener noreferrer"
        >
          {props.transaction.hash}
        </a>
      </span>
    </td>
    <td class="constrained">
      <span>{props.transaction.blockNumber}</span>
    </td>
    <td class="constrained">
      <span>
        <a
          href={etherscanAddressUrlBase + props.transaction.from}
          target="_blank"
          rel="noopener noreferrer"
        >
          {props.transaction.from}
        </a>
      </span>
    </td>
    <td class="constrained">
      <span>
        <a
          href={etherscanAddressUrlBase + props.transaction.to}
          target="_blank"
          rel="noopener noreferrer"
        >
          {props.transaction.to}
        </a>
      </span>
    </td>
    <td>{props.transaction.value / 1000000000000000000} Ether</td>
  </tr>
);

export default Transaction;
