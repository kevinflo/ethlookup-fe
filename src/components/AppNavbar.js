import React from "react";
import { Navbar } from "react-bootstrap";
import { Link } from "react-router-dom";

export default () => {
  return (
    <Navbar>
      <Navbar.Header>
        <Navbar.Brand>
          <Link to="/">Ethlookup.com</Link>
        </Navbar.Brand>
      </Navbar.Header>
    </Navbar>
  );
};
