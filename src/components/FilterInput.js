import React, { Component } from "react";
import * as accountActions from "../actions/accountActions";
import { withRouter } from "react-router-dom";
import { Field, reduxForm } from "redux-form";
import { connect } from "react-redux";
import { Button, Form, FormControl } from "react-bootstrap";
import { bindActionCreators } from "redux";

const validate = values => {
  const errors = {};
  if (values.filterAddress && values.filterAddress.length !== 42) {
    errors["filterAddress"] = "Incorrect address length";
  }

  return errors;
};

const renderInput = ({
  input,
  label,
  placeholder,
  id,
  meta: { touched, error, submitted },
  ...custom
}) => {
  return (
    <div>
      <FormControl
        margin="normal"
        label={label}
        {...input}
        {...custom}
        type="text"
        placeholder={placeholder}
        id={id}
      />
      {touched && error && submitted && <span color="error">{error}</span>}
    </div>
  );
};

class FilterInput extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  handleSubmit(event) {
    event.preventDefault();
    const { filterAddress } = this.props;
    const ethAddress = this.props.match.params.address;

    this.props.actions.loadAccount(ethAddress, filterAddress);
  }

  render() {
    const { invalid, pristine } = this.props;

    return (
      <Form onSubmit={this.handleSubmit.bind(this)}>
        <Field
          component={renderInput}
          name="filterAddress"
          id="eth-address"
          type="text"
          placeholder="Ethereum address (ex: 0xa64CeC5f0247da31A6212709EbAc53b9d3b9B0e3)"
          bsSize="small"
        />

        <Button
          id="btn-search"
          bsStyle="primary"
          type="submit"
          disabled={pristine || invalid}
        >
          Filter
        </Button>
      </Form>
    );
  }
}

function mapStateToProps(state, props) {
  return {
    filterAddress:
      state.form.filterInput &&
      state.form.filterInput.values &&
      state.form.filterInput.values.filterAddress
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(accountActions, dispatch)
  };
}

export default withRouter(
  reduxForm({
    form: "filterInput",
    validate
  })(
    connect(
      mapStateToProps,
      mapDispatchToProps
    )(FilterInput)
  )
);
